"""
              Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module case.py
    ce module teste les cases du plateau
"""
import trojan
import protection
import equipement
import case


def creer_5_cases():
    res={}
    c1=case.creer_case("H",None,None,[])
    res[1]={"case":c1,"fleche":'H',"protection":None,"serveur":None,"presents":[]}
    p=protection.creer_protection(protection.DPO,2)
    c2=case.creer_case("",p,None,[])
    res[2]={"case":c2,"fleche":'',"protection":p,"serveur":None,"presents":[]}
    e=equipement.creer_equipement(equipement.SERVEUR,10)
    c3=case.creer_case("",None,e,[])
    res[3]={"case":c3,"fleche":'',"protection":None,"serveur":e,"presents":[]}
    lt=[trojan.creer_trojan(1,3,'G'),trojan.creer_trojan(2,0,'H')]
    c4=case.creer_case("",None,None,lt)
    res[4]={"case":c4,"fleche":'',"protection":None,"serveur":None,"presents":lt.copy()}
    p=protection.creer_protection(protection.DONNEES_PERSONNELLES,1)
    c5=case.creer_case("G",p,None,[])
    res[5]={"case":c5,"fleche":'G',"protection":p,"serveur":None,"presents":[]}
    return res


def test_get_fleche():
    res=creer_5_cases()
    for elem in res.values():
        assert case.get_fleche(elem["case"])==elem["fleche"]


def get_protection():
    res=creer_5_cases()
    for elem in res.values():
        assert case.get_protection(elem["case"])==elem["protection"]


def test_get_serveur():
    res=creer_5_cases()
    for elem in res.values():
        assert case.get_serveur(elem["case"])==elem["serveur"]

def test_get_trojans():
    res=creer_5_cases()
    for elem in res.values():
        assert case.get_trojans(elem["case"])==elem["presents"]
        

def test_get_trojans_entrants():
    res=creer_5_cases()
    for elem in res.values():
        assert case.get_trojans_entrants(elem["case"])==[]

def test_set_fleche():
    res=creer_5_cases()
    dir="HBGDHGDB"
    i=0
    for elem in res.values():
        case.set_fleche(elem["case"],dir[i])
        assert case.get_fleche(elem["case"])==dir[i]
        i+=1


def test_set_serveur():
    res=creer_5_cases()
    resistance=10
    for elem in res.values():
        s=equipement.creer_equipement(equipement.SERVEUR,resistance)
        case.set_serveur(elem["case"],s)
        assert case.get_serveur(elem["case"])==s
        resistance-=1


def test_set_protection():
    res=creer_5_cases()
    i=0
    for elem in res.values():
        p=protection.creer_protection(i,2)
        case.set_protection(elem["case"],p)
        assert case.get_protection(elem["case"])==p
        i+=1

def test_set_les_trojans():
    res=creer_5_cases()
    l1=[trojan.creer_trojan(1,0,'G'),trojan.creer_trojan(2,1,'D'),
        trojan.creer_trojan(1,4,'H'),trojan.creer_trojan(3,3,'B'),trojan.creer_trojan(3,0,'G')]
    l2=[]
    for elem in res.values():
        case.set_les_trojans(elem["case"],l1.copy(),l2.copy())
        assert case.get_trojans(elem["case"])==l1 and case.get_trojans_entrants(elem["case"])==l2
        l2.append(l1.pop())
    


def test_ajouter_trojan():
    res=creer_5_cases()
    t1=trojan.creer_trojan(1,0,'G')
    t2=trojan.creer_trojan(3,3,'B')
    for elem in res.values():
        case.ajouter_trojan(elem["case"],t1)
        assert case.get_trojans_entrants(elem["case"])==[t1]
        case.ajouter_trojan(elem["case"],t2)
        assert case.get_trojans_entrants(elem["case"])==[t1,t2]


def test_mettre_a_jour_case():
    res=creer_5_cases()
    # premiere case
    t1=trojan.creer_trojan(1,0,'G')
    t2=trojan.creer_trojan(3,3,'B')
    c=res[1]["case"]
    case.ajouter_trojan(c,t1)
    case.ajouter_trojan(c,t2)
    t1a=trojan.creer_trojan(1,0,'H')
    t2a=trojan.creer_trojan(3,3,'H')
    le_resultat=case.mettre_a_jour_case(c)
    assert le_resultat=={1:1,2:0,3:1,4:0}
    assert case.get_trojans(c) in [[t1a,t2a],[t2a,t1a]]
    assert case.get_trojans_entrants(c)==[]
    
    # troisieme case
    c=res[3]["case"]
    le_resultat=case.mettre_a_jour_case(c)
    assert le_resultat=={1:0,2:0,3:0,4:0}
    assert case.get_trojans(c)== []
    assert case.get_trojans_entrants(c)==[]
    assert equipement.get_resistance(case.get_serveur(c))==10
    # quatrieme case
    t1=trojan.creer_trojan(1,0,'G')
    t2=trojan.creer_trojan(3,3,'B')
    c=res[4]["case"]
    case.ajouter_trojan(c,t1)
    case.ajouter_trojan(c,t2)
    le_resultat=case.mettre_a_jour_case(c)
    assert le_resultat=={1:1,2:0,3:1,4:0}
    liste_presents=case.get_trojans(c)
    assert liste_presents in ([t1,t2],[t2,t1])
    assert case.get_trojans_entrants(c)==[]



def test_poser_avatar():
    res=creer_5_cases()
    for elem in res.values():
        le_res=case.poser_avatar(elem["case"])
        assert case.contient_avatar(elem["case"])
        if len(elem["presents"])==0:
            assert le_res=={1:0,2:0,3:0,4:0}
        else:
            assert le_res=={1:1,2:1,3:0,4:0}

def test_enlever_avatar():
    res=creer_5_cases()
    for elem in res.values():
        case.enlever_avatar(elem["case"])
        assert not case.contient_avatar(elem["case"])


def test_contient_avatar():
    res=creer_5_cases()
    for elem in res.values():
        assert not case.contient_avatar(elem["case"])        


def test_reinit_trojans_entrants():
    res=creer_5_cases()
    for elem in res.values():
        case.ajouter_trojan(elem["case"],trojan.creer_trojan(1,1,'G'))
        case.reinit_trojans_entrants(elem["case"])
        assert case.get_trojans_entrants(elem["case"])==[] 

