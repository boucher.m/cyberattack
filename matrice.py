"""
             Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module matrice.py
    Ce module de gestion des matrices
"""


def creer_matrice(nb_lig, nb_col, val_defaut=None):
    """créer une matrice contenant nb_lig lignes et nb_col colonnes avec
       pour valeur par défaut val_defaut

    Args:
        nb_lig (int): un entier strictement positif
        nb_col (int): un entier strictement positif
        val_defaut (Any, optional): La valeur par défaut des éléments de la matrice.
                                    Defaults to None.
    Returns:
        dict: la matrice
    """
    return {'nb_lig': nb_lig, 'nb_col': nb_col, 'valeurs': [val_defaut] * (nb_lig * nb_col)}



def get_nb_lignes(matrice):
    """retourne le nombre de lignes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de lignes de la matrice
    """
    return matrice['nb_lig']


def get_nb_colonnes(matrice):
    """retourne le nombre de colonnes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de colonnes de la matrice
    """
    return matrice['nb_col']


def get_val(matrice, lig, col):
    """retourne la valeur en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)

    Returns:
        Any: la valeur en lig, col de la matrice
        ou None si valeur out of range
    """
    valeur = None
    if lig < matrice['nb_lig'] and col < matrice['nb_col']: # permet de vérifier que les coordonnées soient bien dans la matrice afin d'éviter une erreur out of range
        valeur = matrice['valeurs'][lig * matrice["nb_col"] + col]
    return valeur



def set_val(matrice, lig, col, val):
    """stocke la valeur val en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)
        val (Any): la valeur à stocker
    """
    matrice['valeurs'][lig * matrice["nb_col"] + col] = val


def max_matrice(matrice, interdits=None):
    """retourne la liste des coordonnées des cases contenant la valeur la plus grande de la matrice
        Ces case ne doivent pas être parmi les interdits.

    Args:
        matrice (dict): une matrice
        interdits (set): un ensemble de tuples (ligne,colonne) de case interdites. Defaults to None

    Returns:
        list: la liste des coordonnées de cases de valeur maximale dans la matrice (hors cases interdites)
    """
    nb_lignes = get_nb_lignes(matrice)
    nb_colonnes = get_nb_colonnes(matrice)
    liste_max = []
    valeur = matrice['valeurs']
    valeurs = valeur.copy()
    val_max = max(valeurs)
    for ligne in range(nb_lignes):
        for colonne in range(nb_colonnes):
            if get_val(matrice, ligne, colonne) == val_max:
                liste_max.append((ligne, colonne))
                if interdits != None and (ligne, colonne) in interdits:
                    liste_max.remove((ligne, colonne))
                    valeurs.remove(val_max)
                    val_max = max(valeurs)
    return liste_max





DICO_DIR = {(-1, 1): 'HD', (-1, 0): 'HH', (-1, -1): 'HG', (0, -1): 'GG',
             (0, 1): 'DD', (1, -1): 'BG', (1, 0): 'BB', (1, 1): 'BD'}

def milieu(matrice):
    return get_val(matrice, matrice['nb_lig']//2, matrice['nb_col']//2)


def vers_milieu(matrice, ligne, colonne):
    ligne_milieu = get_nb_lignes(matrice)//2
    colonne_milieu = get_nb_colonnes(matrice)//2
    if ligne_milieu < ligne and colonne_milieu > colonne:
        return 'HD'
    elif ligne_milieu < ligne and colonne_milieu == colonne:
        return 'HH'
    elif ligne_milieu < ligne and colonne_milieu < colonne:
        return 'HG'
    elif ligne_milieu == ligne and colonne_milieu < colonne:
        return 'GG'
    elif ligne_milieu == ligne and colonne_milieu > colonne:
        return 'DD'
    elif ligne_milieu > ligne and colonne_milieu < colonne:
        return 'BG'
    elif ligne_milieu > ligne and colonne_milieu == colonne:
        return 'BB'
    elif ligne_milieu > ligne and colonne_milieu > colonne:
        return 'BD'



def plus_grand_voisin(matrice, ligne, colonne):
    max_vois = 0 # j'initialise à 0 plutôt que None car dans le cas où tous les voisins étaient à 0 la fonction renvoyait None plutôt que 0
    for lig in [-1, 0, 1]:
        for col in [-1, 0, 1]:
            if (get_val(matrice, ligne+lig, colonne+col) is not None) and (get_val(matrice, ligne+lig, colonne+col) != get_val(matrice, ligne, colonne)) and (max_vois < get_val(matrice, ligne+lig, colonne+col)): # (get_val(matrice, ligne+lig, colonne+col) != get_val(matrice, ligne, colonne)) permet d'éviter la case elle même et de ne prendre en compte que ses voisins
                max_vois = get_val(matrice, ligne+lig, colonne+col)
    return max_vois

def direction_max_voisin(matrice, ligne, colonne):
    """retourne la liste des directions qui permettent d'aller vers la case voisine de 
       la case (ligne,colonne) la plus grande. Le résultat doit aussi contenir la 
       direction qui permet de se rapprocher du milieu de la matrice
       si ligne,colonne n'est pas le milieu de la matrice

    Args:
        matrice (dict): une matrice
        ligne (int): le numéro de la ligne de la case considérée
        colonne (int): le numéro de la colonne de la case considérée

    Returns:
        str: deux lettres indiquant la direction DD -> droite , HD -> Haut Droite,
                                                 HH -> Haut, HG -> Haut gauche,
                                                 GG -> Gauche, BG -> Bas Gauche, BB -> Bas, BD -> bas droit 
    """
    max_voisin = plus_grand_voisin(matrice, ligne, colonne)
    res = []
    if get_val(matrice, ligne , colonne) != milieu(matrice):
        res.append(vers_milieu(matrice, ligne, colonne))
    if max_voisin == get_val(matrice, ligne-1, colonne+1) and 'HD' not in res:
        res.append('HD')
    if max_voisin == get_val(matrice, ligne-1, colonne) and 'HH' not in res:
        res.append('HH')
    if max_voisin == get_val(matrice, ligne-1, colonne-1) and 'HG' not in res:
        res.append('HG')
    if max_voisin == get_val(matrice, ligne, colonne-1) and 'GG' not in res:
        res.append('GG')
    if max_voisin == get_val(matrice, ligne, colonne+1) and 'DD' not in res:
        res.append('DD')
    if max_voisin == get_val(matrice, ligne+1, colonne-1) and 'BG' not in res:
        res.append('BG')
    if max_voisin == get_val(matrice, ligne+1, colonne) and 'BB' not in res:
        res.append('BB')
    if max_voisin == get_val(matrice, ligne+1, colonne+1) and 'BD' not in res:
        res.append('BD')
    return res
